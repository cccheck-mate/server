use app::App;

/// Dummy application
///
/// An application implementation that does exactly nothing.
pub struct Dummy {}

impl Dummy {
    /// Construct this application
    pub fn new() -> Dummy {
        Dummy {}
    }
}

impl App for Dummy {
    fn init(&mut self) {
        println!("Dummy app has started.");
    }

    fn terminate(&mut self) {
        println!("Dummy app has terminated.");
    }
}
