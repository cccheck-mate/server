use app::App;
use apps::Dummy;

/// Server core
///
/// This manages the running app, the connection with peripherals and so on.
pub struct Core {
    app: Box<App>,
}

impl Core {
    /// Construct the core
    ///
    /// This will construct a new core instance and set everything up.
    /// The [`Dummy`](Dummy) application is loaded by default.
    pub fn new() -> Core {
        Core::default()
    }

    /// Load an application
    ///
    /// This will load a given initialised application instance.
    /// Any existing application instance will be terminated gracefully.
    pub fn load(&mut self, app: Box<App>) {
        // Terminate the current application
        self.app.terminate();

        // Swap the active application, and initialize
        self.app = app;
        self.app.init();
    }
}

impl Default for Core {
    fn default() -> Core {
        // Initialize the core
        let mut core = Core {
            app: Box::new(Dummy::new()),
        };

        // On first initialization applications must be explicity initialized
        core.app.init();

        core
    }
}

impl Drop for Core {
    /// Gracefully terminate apps on drop
    ///
    /// Ensure that any running app is gracefully terminated when the core instance is dropped.
    fn drop(&mut self) {
        self.app.terminate();
    }
}
