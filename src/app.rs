/// Generic application type
///
/// Applications will implement this generic trait to allow easy management of apps by the
/// [`Core`][Core].
pub trait App {
    /// Initialize the application
    ///
    /// This is invoked when the application is initialized and loaded in the core.
    fn init(&mut self);

    /// Terminate the application
    ///
    /// This is invoked when the application is terminated, when it is removed from the core.
    fn terminate(&mut self);
}
