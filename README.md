# CCCheck-mate server
The server implementation that runs on an Raspberry Pi that manages the
application and game state, and ties the peripherals together.

Please refer to the [main repository][main-repo] for more information.

[main-repo]: https://gitlab.com/cccheck-mate/cccheck-mate
